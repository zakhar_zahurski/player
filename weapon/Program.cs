﻿class MainClass
{
    public static void Main(string[] args)
    {
        
        Knife knife = new Knife();
        RapidFireGun rapidFireGun = new RapidFireGun();
        Gun gun = new Gun();
        Pistol pistol = new Pistol();

        Player player1 = new Player("Tom", 100);
        Player player2 = new Player("Bob", 100);
        Player player3 = new Player("Sad", 100);
        Player player4 = new Player("Lie", 100);

        player1.ShowPlayer();
        player1.SetWeapon(GetRandomWeapon());
        player1.ShowWeapon();

        player2.ShowPlayer();
        player2.SetWeapon(GetRandomWeapon());
        player2.ShowWeapon();

        player3.ShowPlayer();
        player3.SetWeapon(GetRandomWeapon());
        player3.ShowWeapon();

        player4.ShowPlayer(); 
        player4.SetWeapon(GetRandomWeapon());
        player4.ShowWeapon();

        Console.WriteLine();

    }

    public static Weapon GetRandomWeapon()
    {
        Random random = new Random();
        Weapon randomWeapon = new Weapon();

        switch (random.Next(0, 3))
        {
            case 0:
                randomWeapon = new Knife();
                break;
            case 1:
                randomWeapon = new Gun();
                break;
            case 2:
                randomWeapon = new RapidFireGun();
                break;
            case 3:
                randomWeapon = new Pistol();
                break;
        }
        return randomWeapon;
    }
}
class Player
{
    protected Weapon weapon = new Knife();
    protected string Name;
    private int moveSpeed = 100;
    private int rotateSpeed = 50;
    protected int Health = 100;

    public Player(string name, int health)
    {
        Name = name;
        Health = health;
    }
    public void ShowWeapon()
    {
        this.weapon.showGun();
    }
    public void ShowPlayer()
    {
        Console.WriteLine($"\n__________\nИмя: {Name}\nHP: {Health}");
    }
    public void SetWeapon(Weapon weapon)
    {
        this.weapon = weapon;
    }

}

class Weapon
{
    protected string gunName;
    protected int damage;
    protected int clip;
    protected int damageDistance;
    public virtual void showGun()
    {
        Console.WriteLine($"");
    }
}

class Knife:Weapon
{
    public Knife()
    {
        gunName = "Knife";
        damage = 35;
        clip = 1;
        damageDistance = 40;
    }
    public override void showGun()
    {
        base.showGun();
        Console.WriteLine($"Оружие: {gunName}\nУрон: {damage}\nМагазин: {clip}");
    }
}

class RapidFireGun : Weapon
{
    public RapidFireGun()
    {
        gunName = "MP5";
        damage = 20;
        clip = 30;
        damageDistance = 500;
    }
    public override void showGun()
    {
        base.showGun();
        Console.WriteLine($"Оружие: {gunName}\nУрон: {damage}\nМагазин: {clip}");
    }
}

class Gun : Weapon
{
    public Gun()
    {
        gunName = "AK-74";
        damage = 35;
        clip = 30;
        damageDistance = 900;
    }
    public override void showGun()
    {
        base.showGun();
        Console.WriteLine($"Оружие: {gunName}\nУрон: {damage}\nМагазин: {clip}");
    }
}

class Pistol : Weapon
{
    public Pistol()
    {
        gunName = "Desert Eagle";
        damage = 45;
        clip = 7;
        damageDistance = 900;
    }
    public override void showGun()
    {
        base.showGun();
        Console.WriteLine($"Оружие: {gunName}\nУрон: {damage}\nМагазин: {clip}");
    }
}
